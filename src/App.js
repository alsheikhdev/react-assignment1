import React, { Component } from 'react';
import './App.css';

import UserInput from './UserInput/UserInput'
import './UserInput/UserInput.css';

import UserOutPut from './UserOutPut/UserOutPut'
import './UserOutPut/UserOutPut.css';



class App extends Component {

  state = {
    username: ""
  }

  userNameChangeHandler = (event) => {
    //console.log(event.target.value);
    this.setState({username: event.target.value});
  }

  render() {
    return (
      <div className="App">
        <p>Welcome to assignment 1 solution!</p>
        <UserInput changed={this.userNameChangeHandler}/>
        <br/>
        <UserOutPut username={this.state.username}/>
      </div>
    );
  }

}

export default App;

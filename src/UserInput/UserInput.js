import React, { Component } from 'react';
import "./UserInput.css"

class UserInput extends Component {

  render() {
    return (
      <div className="UserInPut">
        <p>Please enter your name</p>
        <input type="text" onChange={this.props.changed}/>
      </div>
    );
  }


}

export default UserInput;

import React, { Component } from 'react';
import "./UserOutPut.css"

class UserOutPut extends Component {

  render() {
    return (
        <div className="UserOutPut">
            <p><strong>{this.props.username}</strong></p>
        </div>
    );
  }

}

export default UserOutPut;
